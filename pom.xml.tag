<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">

  <modelVersion>4.0.0</modelVersion>
  <groupId>com.liangyuen.web</groupId>
  <artifactId>Jogging</artifactId>
  <packaging>war</packaging>
  <version>1.0.0</version>
  <name>Jogging Maven Webapp</name>
  <url>http://maven.apache.org</url>
  <dependencies>
    <dependency>
      <groupId>junit</groupId>
      <artifactId>junit</artifactId>
      <version>3.8.1</version>
      <scope>test</scope>
    </dependency>
    <dependency>
    	<groupId>javax.servlet</groupId>
    	<artifactId>javax.servlet-api</artifactId>
    	<version>3.1.0</version>
    	<scope>provided</scope>
    </dependency>
  </dependencies>
  <build>
    <finalName>Jogging</finalName>
    <plugins>
    	<plugin>
    		<groupId>org.apache.maven.plugins</groupId>
    		<artifactId>maven-compiler-plugin</artifactId>
    		<version>3.3</version>
    		<configuration>
                <source>1.8</source>
          		<target>1.8</target>
        	</configuration>
    	</plugin>
    	<plugin>
    		<groupId>org.apache.maven.plugins</groupId>
    		<artifactId>maven-war-plugin</artifactId>
    		<version>2.6</version>
    	</plugin>
    	<plugin>
    		<groupId>org.apache.maven.plugins</groupId>
    		<artifactId>maven-install-plugin</artifactId>
    		<version>2.5.2</version>
    	</plugin>
    	<plugin>
    		<groupId>org.apache.maven.plugins</groupId>
    		<artifactId>maven-resources-plugin</artifactId>
    		<version>2.7</version>
    	</plugin>
    	<plugin>
    		<groupId>org.apache.maven.plugins</groupId>
    		<artifactId>maven-deploy-plugin</artifactId>
    		<version>2.8.2</version>
    	</plugin>
    	<plugin>
    		<groupId>org.apache.maven.plugins</groupId>
    		<artifactId>maven-release-plugin</artifactId>
    		<version>2.5.3</version>
    		<configuration>
				<tagBase>https://code.csdn.net/abc516/jogging.git</tagBase>
			</configuration>
    	</plugin>
    	<plugin>
    		<groupId>org.apache.maven.plugins</groupId>
    		<artifactId>maven-scm-plugin</artifactId>
    		<version>1.9.4</version>
    		<configuration>
          		<connectionType>developerConnection</connectionType>
        	</configuration>
    	</plugin>
    </plugins>
  </build>
  
  <distributionManagement>    
    <repository>    
      <id>Jogging-releases</id>    
      <name>Jogging Release Repository</name>    
      <url>https://bitbucket.org/abc516/jogging</url>    
    </repository>    
    <snapshotRepository>    
      <id>Jogging-snapshots</id>    
      <name>Jogging Snapshot Repository</name>    
      <url>https://code.csdn.net/abc516/jogging.git</url>    
    </snapshotRepository>    
  </distributionManagement>    
  
  <scm>
	<connection>scm:git:https://bitbucket.org/abc516/jogging</connection>
  	<developerConnection>scm:git:https://code.csdn.net/abc516/jogging.git</developerConnection>
  	
    <tag>Jogging-1.0.0</tag>
  </scm>
  
</project>
